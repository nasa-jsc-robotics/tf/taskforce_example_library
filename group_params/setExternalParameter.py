"""
Task Template

To create a custom Task:
- change the name of the task
- (optional) add events to the 'events' variable
- (optional) add commands to the 'commands' variable
- (optional) add parameters to the 'parameters' variable
- (optional) insert custom code by replacing the "INSERT USER CODE HERE" comment blocks.
  Any methods that don't need to customize / overload, can safely be deleted.

To log messages, the component has a member logger which uses the python logger.  Logging should be configured at the
application level.  The TaskEngine uses taskforce_common.logcfg module to configure logging.

    Example:
        self.logger.warning('Warning message!')
"""

# standard imports.  Add more imports as needed.
from taskforce_common import Task, TaskState, Response, ErrorResponse


# TODO: Change the name of the class to match the filename
class setExternalParameter(Task):
    """
    Events:
    -------
    Add emittable events as a list of strings in the class variable called "events".  By default, an event will be
    emitted after the onDeploy, onStartup, onExecute, and onStop hooks.

    Example:
        events = ['error', 'trajComple', 'etc']

    To emit an event, call:
        self.emit(<event name>)

    Example:
        self.emit('success')

    NOTE: Generally, you will only want to emit events in the onExecute hook.  Emitting events in other methods is
          possible, it could lead to non-intuitive execution.  Instead of explicitly emitting a "complete" event, for
          example, let the task exit the onExecute hook by returning True.

    Commands:
    ---------
    Add custom commands by putting them method name in the "commands" array.  This method
    will now be callable through the Task's command handler.

    Example:
        commands = ['myMethod']

    Parameters:
    ----------
    Add custom parameters by adding values to the 'parameters' dictionary.  These values can be
    accessed at run-time by accessing 'self.parameters' or using the base class's 'setParameters' / 'getParameters'.

    NOTE: Do not add / delete parameters that have not been declared in the 'parameters' dictionary!

    Example:
        parameters = {'color' : 'blue', 'size' : 'large'}

    """
    events = ['changeYourParamsNow']
    commands = []
    parameters = {'genre': 'whatever'}

    def onDeploy(self):
        """
        The onDeploy method will get called when the Task is deployed to the Task engine.  This can be used in place
        of what would normally go in the Constructor.  Put things like member variable initialization or other "eager"
        initialization methods here.  If a more "lazy" initialization is preferred, move code to the "onInit" method.
        """

        ########################
        #  INSERT USER CODE HERE
        ########################

        return True

    def onInit(self):
        """
        The onInit method will get called when the Task.init() method is called.  Typically,
        this only gets called once per deployment.  This is where you would register publishers / subscribers,
        or do one-time setup functions.  If initialization is successful, this method should return True.
        On error, it should return False.  onInit is also invokable at run-time, so place code here
        that needs to be re-inited, since you can't re-run the constructor.  When Task.init() is called, and if this
        method returns "True", an "initialized" event will be emitted.
        """

        ########################
        #  INSERT USER CODE HERE
        ########################

        return True

    def onStartup(self):
        """
        The onStartup method will get called when the Task.startup() method is called.  If the Task has not been
        initialized, the Task will first invoke it's init() (and then onInit() hook).

        This is for functions that need to be run every time the Task is run, but maybe not in a continuous loop. If
        onStartup is successful, it should return True.  On error, it should return False.  If this method returns True,
        the task will emit a "startup" event, and then the Tasks onExecute hook will be called.  If this method returns
        False, the Task will go into it's FAILED state, and invoke the onFail() hook.
        """
        ########################
        #  INSERT USER CODE HERE
        ########################

        return True

    def onExecute(self):
        """
        The onExecute method is the "main" loop of the Task.  When a task is started by calling Task.startup(), it will
        first call onStartup.  If onStartup is successful, the onExecute method will be called.

        If this method completes without error, it should return True.  If there is an error, it should return False.
        If this method returns False, the Task will go to the FAILED state and call the onFail hook.  If it returns
        true while the Task was in the RuNNING state, the task will emit a "complete" event.
        """

        # if onExecute needs to run continuously, put the code in the following loop
        #while (self.state == TaskState.RUNNING):

        ########################
        #  INSERT USER CODE HERE
        ########################
        
        self.emit('changeYourParamsNow', self.parameters)

        return True

    def onStop(self):
        """
        The onStop method is called when a Task.stop() is called.  Calling stop() will not necessarily cause execution
        to end.  All it will do is put the Task in the STOPPED state.  This can be used in the onExecute loop to react
        to a desire to stop.  When this method returns, it will emit a "stopped" event, regardless of the return value.
        """

        ########################
        #  INSERT USER CODE HERE
        ########################

        return True

    def onFail(self):
        """
        The onFail method is called when the other hooks return false.  This hook gives the opportunity to do error
        handling, recovery, or maybe extra debugging.  This method does not explicitly emit any events.
        """

        ########################
        #  INSERT USER CODE HERE
        ########################

        return True

    def onShutdown(self):
        """
        The onShutdown method is called when a task is being removed completely. Code needed to clean up or special
        destructors should be placed here.
        """

        ########################
        #  INSERT USER CODE HERE
        ########################

        return True
