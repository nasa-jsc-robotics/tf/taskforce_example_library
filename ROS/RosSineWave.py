"""
RosSineWave

To create a custom Task, simply change the name of the task, add events to 
the 'events' variable and insert custom code by replacing the "INSERT USER
CODE HERE" comment blocks.  Any of the default method below that don't 
need to be customized can be deleted.
"""

# standard imports.  Add more imports as needed.
from taskforce_common import Task, TaskState, Response, ErrorResponse
from taskforce_plugin_ros import RosTask

import math
import rospy
from std_msgs.msg import Float64

# Change the name of the task
class RosSineWave(RosTask):
    """
    Add emittable events as a list of strings in a
    class variable called "events".  By default,
    an event will be emitted after onStartup, onStop,
    and when a task completes.

    Example:
        events = ['error', 'trajComple', 'etc']

    To emit and event, call:
        self.emit(<event name>)

    Example:
        self.emit('complete')
    """
    events = []

    """
    To make any method callable through the command interface,
    add the name of the method to the "commands" list.  The method
    must have the form:

    def methodName(self, message):

    Where message is a taskforce_common.Message or taskforce_common.Command
    The method must return an taskforce_common.Response on success or taskforce_common.ErrorResponse on failure.

        commands = ['customMethod1','customMethod2']

    Example:

        commands = ['customMethod1']
        ...
        ...
        def customMethod1(self,message):
            something = doSomethingAwesome(message.data)

            if something == Awesome:
                return Response(message.name,data='man, that was AWESOME!')
            else:
                return ErrorResponse('Something went totally wrong.  NOT awesome...')
    """
    commands = []
    
    parameters = {'amplitude':1.0, 'freq':1.0, 'offset':0.0}

    def __init__(self, name, commandPorts=[]):
        RosTask.__init__(self,name,commandPorts)

        # uncomment to change the default logger
        # By default, it will log to syslog, the console, and a remote log server
        # self.logger = logcfg.getLogger(name=self.name, level=None, syslog=True, console=True, remote=True)

        self.start_time = 0
        self.stop_time = None

    def onInit(self):
        """
        The onInit method will get called when the Task is initialized.  Typically,
        this only gets called once per deployment.  This is where you would register publishers / subscribers,
        or do one-time setup functions.  If init is successful, it should return True.
        On error, it should return False.
        """
        RosTask.onInit(self)

        self.topic = self.name + '/sinewave'
        self.publisher = rospy.Publisher(self.topic, Float64)
        self.start_time = 0
        self.stop_time = None

        return True

    def onStartup(self):
        """
        The onStartup method will get called when a Task is started.  This is
        for functions that needs to be run everytime the Task is run, but
        maybe not in a continuous loop. If onStartup is successful, it should
        return True.  On error, it should return False.
        """
        if self.stop_time == None:
            self.start_time = rospy.get_time()
        else:
            self.start_time = self.stop_time

        self.logger.debug('Start time:{}'.format(self.start_time))
        return True

    def onExecute(self):
        """
        The onExecute method is the "main" loop of the Task.  When a task is
        started, if will first call onStartup.  If onStartup is successful,
        the onExecute method will be called in it's own thread.  If a task
        is currently running, more onExecute threads will NOT be created.
        """

        msg = Float64()
        # if onExecute needs to run continuously, put the code in the following loop
        while (self.state == TaskState.RUNNING):
            elapsed_time = rospy.get_time() - self.start_time
            value = self.getSineValue(elapsed_time, self.parameters['amplitude'], self.parameters['offset'], self.parameters['freq'])
            msg.data = value
            self.publisher.publish(msg)
            rospy.sleep(0.01)
            self.customStatus = '{:6.4f}'.format(value)
        return True

    def onStop(self):
        """
        The onStop method is called when a task is manually told to stop.
        """

        self.stop_time = rospy.get_time()
        return True

    def onShutdown(self):
        """
        The onShutdown method is called when a task is being removed completely.
        Code needed to clean up or special destructors should be placed here.
        """

        # here, we need to unregister all publishers and subscribers.
        # e.g.:
        # 	self.publisher.unregister()
        self.stop()
        if hasattr(self,'publisher'):
            self.publisher.unregister()
        return True

    def getSineValue(self, elapsedTime_sec, amplitude, offset, freq_hz):
        self.logger.debug('time   = {}'.format(elapsedTime_sec))
        self.logger.debug('amp    = {}'.format(amplitude))
        self.logger.debug('offset = {}'.format(offset))
        self.logger.debug('freq_hz= {}'.format(freq_hz))
        return amplitude*math.sin(elapsedTime_sec * 2 * math.pi * freq_hz) + offset