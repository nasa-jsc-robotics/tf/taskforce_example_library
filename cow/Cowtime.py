# standard imports.  Add more imports as needed.
import subprocess
import time

from taskforce_common import Task, TaskState, Response, ErrorResponse

class Cowtime(Task):
    """
    This Task will run the "cowsay" command program.  This Task is 
    mainly used for testing.
    """
    
    events = []
    commands = []

    def __init__(self, name, commandPorts=[]):
        Task.__init__(self,name,commandPorts)

    def onExecute(self):
        s = '{} says {}'.format(self.name,time.ctime(time.time()))
        try:
            subprocess.call('cowsay {}'.format(s),shell=True)
        except:
            self.logger.warning('Error running cowsay.  Is it installed?')
            return False
        self.customStatus = s
        return True
        
    def onShutdown(self):
        self.logger.info("[{}] Bye, y'all!".format(self.name))
