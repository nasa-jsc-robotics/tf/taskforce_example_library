"""
RTC Task Template

To create a custom Task, simply change the name of the task, add events to 
the 'events' variable and insert custom code by replacing the "INSERT USER
CODE HERE" comment blocks.  Any of the default method below that don't 
need to be customized can be deleted.
"""

# standard imports.  Add more imports as needed.
from taskforce_common import Task, TaskState, Response, ErrorResponse
from collections import deque
import numpy as np
import time


# Change the name of the task
class Timer(Task):
    """
    Add emittable events as a list of strings in a
    class variable called "events".  By default,
    an event will be emitted after onStartup, onStop,
    and when a task completes.

    Example:
        events = ['error', 'trajComple', 'etc']

    To emit and event, call:
        self.emit(<event name>)

    Example:
        self.emit('complete')
    """
    events = []

    """
    To make any method callable through the command interface,
    add the name of the method to the "commands" list.  The method
    must have the form:

    def methodName(self, message):

    Where message is a taskforce_common.Message or taskforce_common.Command
    The method must return an taskforce_common.Response on success or taskforce_common.ErrorResponse on failure.

        commands = ['customMethod1','customMethod2']

    Example:

        commands = ['customMethod1']
        ...
        ...
        def customMethod1(self,message):
            something = doSomethingAwesome(message.data)

            if something == Awesome:
                return Response(message.name,data='man, that was AWESOME!')
            else:
                return ErrorResponse('Something went totally wrong.  NOT awesome...')
    """
    commands = ['startTime','stopTime', 'results']

    def __init__(self, name, commandPorts=[]):
        Task.__init__(self,name,commandPorts)

        self.deque_length = 1000
        self.start_time = time.time()
        self.delta_time = None

    def onInit(self):
        """
        The onInit method will get called when the Task is initialized.  Typically,
        this only gets called once per deployment.  This is where you would register publishers / subscribers,
        or do one-time setup functions.  If init is successful, it should return True.
        On error, it should return False.
        """
        self.delta_time = deque(maxlen=self.deque_length)
        return True

    def onStartup(self):
        """
        The onStartup method will get called when a Task is started.  This is
        for functions that needs to be run everytime the Task is run, but
        maybe not in a continuous loop. If onStartup is successful, it should
        return True.  On error, it should return False.
        """
        return True

    def onExecute(self):
        """
        The onExecute method is the "main" loop of the Task.  When a task is
        started, if will first call onStartup.  If onStartup is successful,
        the onExecute method will be called in it's own thread.  If a task
        is currently running, more onExecute threads will NOT be created.
        """

        # if onExecute needs to run continuously, put the code in the following loop
        #while (self.state == TaskState.RUNNING):
        return True

    def onStop(self):
        """
        The onStop method is called when a task is manually told to stop.
        """
        return True

    def onShutdown(self):
        """
        The onShutdown method is called when a task is being removed completely.
        Code needed to clean up or special destructors should be placed here.
        """

        ########################
        #  INSERT USER CODE HERE
        ########################

        return True

    def startTime(self):
        if self.state == TaskState.DEPLOYED:
            self.init()
        self.start_time = time.time()
        return self.start_time

    def stopTime(self):
        stop_time = time.time()
        delta = stop_time - self.start_time
        if self.delta_time:
            self.delta_time.append(delta)
        self.logger.info("delta={}".format(delta))
        return stop_time

    def results(self):
        if self.delta_time:
            average = np.average(self.delta_time)
            data = {
                'average' : average,
                'rate'    : 1.0 / average,
                'minTime' : np.min(self.delta_time),
                'maxTime' : np.max(self.delta_time),
                'stddev'  : np.std(self.delta_time)
            }
            return data
        return False


