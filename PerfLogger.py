from collections import deque
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import psutil

colors = ['r', 'g', 'b', 'c', 'm', 'k']


class PerfLogger(object):

    def __init__(self, sample_rate=0.5, filter_depth=2):
        self._data = np.zeros((1, psutil.NUM_CPUS))
        self.sample_rate = sample_rate
        self.sample_filter = deque(maxlen=filter_depth)

    def sample(self):
        sample = psutil.cpu_percent(interval=self.sample_rate, percpu=1)
        self.sample_filter.append(sample)
        sample_array = np.array(self.sample_filter)
        sample_avg = np.mean(sample_array, 0)
        self._data = np.vstack((self._data, sample_avg))

    @property
    def data(self):
        return self._data[1:, :]

    def get_data(self):
        return self.data

    def get_perf_avg(self):
        return np.average(self.data)

    def get_perf_std(self):
        return np.std(self.data)

    def get_perf_low(self):
        return np.min(self.data)

    def get_perf_high(self):
        return np.max(self.data)

    def save_data(self, file_name, data=None):
        if data is None:
            data = self.data
        df = pd.DataFrame(data)
        df.to_csv(file_name)

    def plot_data(self, file_name, title=None, data=None):
        if data is None:
            data = self.data
        fig = plt.figure()
        plt.title(title)
        plt.xlabel("Sample")
        plt.ylabel("usage")
        plt.ylim([0, 110])
        plt.grid()  # turn on grid
        plt.gca().set_color_cycle(colors)

        plt.plot(data)
        plt.savefig(file_name, bbox_inches='tight')
