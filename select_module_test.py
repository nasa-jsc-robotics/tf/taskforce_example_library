import argparse
from multiprocessing import Pipe, Process
import pandas as pd
import numpy as np
import os
import select
import sys
import time
import matplotlib.pyplot as plt
from PerfLogger import PerfLogger

class SelectListener(Process):

    def __init__(self, conn):
        super(SelectListener, self).__init__()
        self.conn = conn
        self.running = False

    def run(self):
        self.running = True
        while self.running:
            rd, wr, ex = select.select([self.conn], [], [])
            for d in rd:
                data = d.recv()
                print data
                if data == 'stop':
                    self.running = False


class BusyWaitListener(Process):

    def __init__(self, conn, poll_timeout=0):
        super(BusyWaitListener, self).__init__()
        self.conn = conn
        self.running = False
        self.poll_timeout = poll_timeout

    def run(self):
        self.running = True
        while self.running:
            if self.conn.poll(self.poll_timeout):
                data = self.conn.recv()
                print data
                if data == 'stop':
                    self.running = False


class Publisher(object):

    def __init__(self, conn):
        self.conn = conn

    def send(self, data):
        self.conn.send(data)


def test_idle(path, output, time_stamp, annotation, num_pairs, duration, listener_cls, listener_kwargs={}):
    publishers = []
    subscribers = []

    perf_logger = PerfLogger()

    test_start = time.time()

    for i in range(num_pairs):
        connA, connB = Pipe()

        pub = Publisher(connA)
        sub = listener_cls(connB, **listener_kwargs)
        publishers.append(pub)
        subscribers.append(sub)

    print 'starting:{}'.format(output)
    for sub in subscribers:
        sub.start()
        while not sub.is_alive():
            time.sleep(0.01)

    start = time.time()
    while (time.time() - start) < duration:
        perf_logger.sample()

    print 'stopping:{}'.format(output)
    for pub in publishers:
        pub.send('stop')

    for sub in subscribers:
        sub.join()

    test_stop = time.time()
    data_output = os.path.join(path, time_stamp + '_' + output + '.csv')
    plot_output = os.path.join(path, time_stamp + '_' + output + '.png')
    report_output = os.path.join(path, time_stamp + '_' + output + '.txt')

    report = ''
    with open(report_output, 'w') as f_ptr:
        report += '{}\n'.format(report_output)
        report += 'data: {}\n'.format(data_output)
        report += 'plot: {}\n'.format(plot_output)
        report += 'sample duration: {}\n'.format(duration)
        report += 'total test time: {}\n'.format(test_stop - test_start)
        report += 'sample_period: {} s\n'.format(sample_period)
        report += 'filter_depth: {} samples\n'.format(filter_depth)
        report += 'num_pairs:\n{}\n'.format(num_pairs)
        report += 'annotation:\n{}\n'.format(annotation)
        report += 'avg:  {}\n'.format(perf_logger.get_perf_avg())
        report += 'std:  {}\n'.format(perf_logger.get_perf_std())
        report += 'low:  {}\n'.format(perf_logger.get_perf_low())
        report += 'high: {}\n'.format(perf_logger.get_perf_high())
        report += 'data:\n{}\n'.format(perf_logger.get_data().__repr__())
        f_ptr.write(report)

    perf_logger.save_data(data_output)
    perf_logger.plot_data(plot_output, title='{}\n{}'.format(output,annotation))
    print report
    print perf_logger.get_data()
    return perf_logger

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Test fixture for evaluating task_engine performance")
    parser.add_argument('-d', '--duration', type=float, default=10.0, help="duration to test")
    parser.add_argument('-p', '--path', type=str, default='./output', help="output path")
    parser.add_argument('-o', '--output', type=str, default="output", help="output name for .csv and .png files")
    parser.add_argument('-a', '--annotation', type=str, default="", help="Annotation for the report")

    args, unknown_args = parser.parse_known_args(sys.argv[1:])

    duration = args.duration
    output = args.output
    annotation = args.annotation
    path = args.path

    sample_period = 0.5
    filter_depth = 2

    time_stamp = time.strftime('%Y_%m_%d_%H%M%S')

    NUM_PAIRS = [10, 100, 250]

    data = np.zeros([0,4])

    for i, num_pairs in enumerate(NUM_PAIRS):
        avg = []
        logger = test_idle(path, 'BusyWait_01_{}'.format(num_pairs), time_stamp, "BusyWaitListener, Poll timeout=0.1", num_pairs, duration, BusyWaitListener, {'poll_timeout': 0.1})
        avg.append(logger.get_perf_avg())
        logger = test_idle(path, 'BusyWait_001_{}'.format(num_pairs), time_stamp, "BusyWaitListener, Poll timeout=0.01", num_pairs, duration, BusyWaitListener, {'poll_timeout': 0.01})
        avg.append(logger.get_perf_avg())
        logger = test_idle(path, 'BusyWait_0001_{}'.format(num_pairs), time_stamp, "BusyWaitListener, Poll timeout=0.001", num_pairs, duration, BusyWaitListener, {'poll_timeout': 0.001})
        avg.append(logger.get_perf_avg())
        logger = test_idle(path, 'SelectListener_{}'.format(num_pairs), time_stamp, "SelectListener", num_pairs, duration, SelectListener)
        avg.append(logger.get_perf_avg())
        data = np.vstack([data, avg])

    df = pd.DataFrame(data, index=NUM_PAIRS, columns=['BusyWait_0.1', 'BusyWait_0.01', 'BusyWait_0.001', 'SelectListener'])
    data_file = os.path.join(path, output + '.csv')
    plot_file = os.path.join(path, output + '.png')
    report_file = os.path.join(path, output + '.txt')

    report = ''
    with open(report_file, 'w') as f_ptr:
    	report += '{}\n'.format(report_file)
    	report += '{}\n'.format(df.__repr__())
    	f_ptr.write(report)

    df.to_csv(data_file)
    ax = df.plot(title='{}\n{}'.format('average usage', annotation), ylim=[0,110], grid=True)
    ax.set_xlabel('num pairs')
    ax.set_ylabel('proc usage')
    plt.savefig(plot_file)
    
    print df
    plt.show()
