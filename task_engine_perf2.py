import argparse
import os
import time
import sys

from taskforce_engine import EngineInterface
from taskforce_common_library.lib.ProgramManager import ProgramManager
from taskforce_common.utils import FileMap
from taskforce_common import BLOCK_FILE_EXTENSION
from taskforce_common import GroupDefinitionLoader
from taskforce_plugin_zmq import ZMQClient

from PerfLogger import PerfLogger

pm = ProgramManager()

LIBRARY_ROOT = os.path.dirname(os.path.abspath(__file__))


def start_task_engine():
    pm.startProgram('task_engine', 'task_engine')


def stop_task_engine(block=True):
    pm.stopProgram('task_engine')
    if block:
        while pm.getProgramStatus('task_engine') is 'alive':
            time.sleep(0.1)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Test fixture for evaluating task_engine performance", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-b', '--block_file', type=str, default="taskforce_example_library/cow/two_cows" + BLOCK_FILE_EXTENSION, help="Group file")
    parser.add_argument('-d', '--duration', type=float, default=10.0, help="duration to test")
    parser.add_argument('-p', '--path', type=str, default='./', help="output path")
    parser.add_argument('-o', '--output', type=str, default="output", help="output name for .csv and .png files")
    parser.add_argument('-a', '--annotation', type=str, default="", help="Annotation for the report")
    parser.add_argument('--wait', type=float, default=0.0, help="Wait after deploy to start taking stats")

    args, unknown_args = parser.parse_known_args(sys.argv[1:])

    command_line = ' '.join(sys.argv)

    block_file = args.block_file
    duration = args.duration
    output = args.output
    annotation = args.annotation
    path = args.path
    startup_wait = args.wait

    print block_file

    sample_period = 0.5
    filter_depth = 2

    time_stamp = time.strftime('%Y_%m_%d_%H%M%S')

    perfLogger = PerfLogger(sample_period, filter_depth)

    ###########
    # Setup the library and the engine interface

    fileMap = FileMap(['(.+\.py$)|(.+\.group$)'])
    fileMap.addFolder(LIBRARY_ROOT)
    client = ZMQClient(port=6555)
    engine_interface = EngineInterface(client)
    #
    ############

    # Start the engine
    start_task_engine()

    test_start = time.time()

    ############
    # Load and deploy the block
    definition = GroupDefinitionLoader.loadDefinition(block_file, fileMap.getMap())
    engine_interface.groupDeploy(definition)
    print "wait:{}".format(startup_wait)
    time.sleep(startup_wait)
    #
    ############

    duration = 10
    start_time = time.time()
    print "start sampling"
    while time.time() - start_time < duration:
        perfLogger.sample()
        print "sample..."


    engine_interface.engineShutdown()
    time.sleep(1)
    stop_task_engine()

    test_stop = time.time()

    if not os.path.exists(path):
        os.mkdir(path)

    data_output = os.path.join(path, time_stamp + '_' + output + '.csv')
    plot_output = os.path.join(path, time_stamp + '_' + output + '.png')
    report_output = os.path.join(path, time_stamp + '_' + output + '.txt')

    report = ''
    with open(report_output, 'w') as f_ptr:
        report += '{}\n'.format(report_output)
        report += 'command_line:\n{}\n'.format(command_line)
        report += 'data: {}\n'.format(data_output)
        report += 'plot: {}\n'.format(plot_output)
        report += 'sample duration: {}\n'.format(duration)
        report += 'total test time: {}\n'.format(test_stop - test_start)
        report += 'startup wait: {}\n'.format(startup_wait)
        report += 'sample_period: {} s\n'.format(sample_period)
        report += 'filter_depth: {} samples\n'.format(filter_depth)
        report += 'block_file: {}\n'.format(block_file)
        report += 'annotation:\n{}\n'.format(annotation)
        report += 'avg:  {}\n'.format(perfLogger.get_perf_avg())
        report += 'std:  {}\n'.format(perfLogger.get_perf_std())
        report += 'low:  {}\n'.format(perfLogger.get_perf_low())
        report += 'high: {}\n'.format(perfLogger.get_perf_high())
        report += 'data:\n{}\n'.format(perfLogger.get_data().__repr__())
        f_ptr.write(report)

    perfLogger.save_data(data_output)
    perfLogger.plot_data(plot_output, title="{}\n{}".format(output, annotation))

    print report
    print perfLogger.get_data()
