import logging

logger = logging.getLogger(__name__)

class HelloClass(object):
	
	def saySomething(self, something):
		logger.warn(something)