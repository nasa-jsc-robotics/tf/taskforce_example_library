# standard imports.  Add more imports as needed.
import subprocess
import time
from taskforce_example_library.test.HelloClass import HelloClass

from taskforce_common import Task, TaskState, Response, ErrorResponse

class HelloTask(Task):
    events = []
    commands = []

    userStatus = {}
    
    def __init__(self, name, commandPorts=[]):
        Task.__init__(self,name,commandPorts)

        self.hc = HelloClass()
        
    def onExecute(self):
        self.hc.saySomething('[{}] says hellooo'.format(self.name))
        return True
        
    def onShutdown(self):
        self.logger.info("[{}] Bye, y'all!".format(self.name))
